# The Ultimate Script Collection

This is the guntiNET Service Script collection hosting various utility scripts.

## Table of Contents
The following scripts are currently available

### 📜 `use.sh`
`use.sh` adds aliases for the scripts to your shell session so the utility scripts can be called more easily. If you want them to be autoloaded on login, add the following line to your `.bashrc`
(or related):

```
source /path/to/repo/use.sh
```

### 📜 **Base**
_Base scripts and templates, not intended for direct execution (call it "Framework" if you want)_

| Script | Purpose |
|:------ |:------- |
| `base.sh` | Base used by all other script, provides custom log functions and some handy shortcuts for scripting |
| `skel.sh` | Skeleton for new scripts, contains the import required to 

### 🐳 **Docker**
_Anything Docker-related_

| Script | Alias | Purpose |
|:------ |:----- |:------- |
| `backup.sh` | `gn_docker_backup` | Backs up the content of a Docker volume to the host system as a TAR archive |
| `restore.sh` | `gn_docker_restore` | Unpacks a TAR archive (packed by `backup.sh`) into an existing Docker volume |

## License
Unless specified otherwise, all scripts are licensed under the MIT license. See [LICENSE](./LICENSE) for more information.
