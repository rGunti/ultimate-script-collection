#!/bin/bash
# Colors
L_RED='\033[1;31m'
L_GREEN='\033[1;32m'
L_ORANGE='\033[0;33m'
L_GRAY='\033[0;36m'
WHITE='\033[1;37m'
NC='\033[0m'

function gn_center_text() {
    text="$1"
    cols=${2:-$(tput cols)}

    printf "%*s" $(((${#text}+$cols)/2)) "$text" $(($cols-(${#text}+$cols)/2))
}

function gn_hello() {
    title=$(gn_center_text "$1" 33)

	echo -e "${L_GREEN}"
    curyear=$(date +"%Y")
	cat << EOF
 #####################################
 #      guntiNET Service Script      #
 # --------------------------------- #
 # $title #
 # --------------------------------- #
 #         (c) $curyear, rGunti          #
 #####################################
EOF
	echo -e "${NC}"
}

function gn_debug() {
    if ! [[ -z "${GN_ENABLE_DEBUG}" ]]; then
        echo -e "${L_GRAY} * ${1}${NC}"
    fi
}
function gn_log() {
	echo -e "${L_GREEN} # ${WHITE}${1}${NC}"
}
function gn_query() {
	echo -e -n "${WHITE} > ${WHITE}${1}${NC}"
}
function gn_ask() {
	while true; do
		echo -e -n "${WHITE} ? ${WHITE}${1}${NC} [Y/N] "
		read -n 1 -r yn
		case $yn in
			[Yy]*) echo -e "\b${L_GREEN}Yes${NC}"; return 1;;
			[Nn]*) echo -e "\b${L_RED}No${NC}"; return 2;;
			*) echo -e "\b${L_RED}Please select Y for \"Yes\" or N for \"No\"${NC}";;
		esac
	done
	return 0
}
function gn_warn() {
	echo -e "${L_ORANGE} ! ${WHITE}${1}${NC}"
}
function gn_error() {
	echo -e "${L_RED} X ${WHITE}${1}${NC}"
}

function gn_root_check() {
	if [ "$EUID" -ne 0 ]; then
		gn_error "Please run as root"
		exit 1
	else
		gn_log "Welcome! You are ready to run this script!"
	fi
}

function gn_sanitize_path() {
    readlink -f "$1"
}

