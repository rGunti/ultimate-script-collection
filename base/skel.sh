#!/bin/bash
source "$(dirname $(realpath $0))/../base/base.sh"
gn_hello "My Cool Script"

function print_help() {
    cat << EOF
Usage:
script.sh <SomeParameter>
> Does some cool stuff using <SomeParameter>

script.sh --help
> Shows this help text

Example:
$ ./script.sh --help
EOF
}

if [[ "$1" == "--help" ]]; then
    print_help
    exit 1
fi

# --- Parameters ---

# --- Actual main ---
