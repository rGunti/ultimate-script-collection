#!/bin/bash
base_dir="$(dirname $(realpath $0))"

# Docker
alias gn_docker_backup="bash ${base_dir}/docker/backup.sh"
alias gn_docker_restore="bash ${base_dir}/docker/restore.sh"
