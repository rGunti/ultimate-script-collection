#!/bin/bash
#####################################
#      guntiNET Service Script      #
#             Installer             #
# --------------------------------- #
#         (c) 2021, rGunti          #
#####################################

GIT_URL_SSH="git@gitlab.com:rGunti/ultimate-script-collection.git"
GIT_URL_HTTP="https://gitlab.com/rGunti/ultimate-script-collection.git"
GN_INSTALL_PATH="${GN_INSTALL_PATH:-/usr/bin/gn-scripts}"

#####################################

L_RED='\033[1;31m'
L_GREEN='\033[1;32m'
L_ORANGE='\033[0;33m'
L_GRAY='\033[0;36m'
WHITE='\033[1;37m'
NC='\033[0m'

function gn_debug() {
    if ! [[ -z "${GN_ENABLE_DEBUG}" ]]; then
        echo -e "${L_GRAY} * ${1}${NC}"
    fi
}
function gn_log() {
	echo -e "${L_GREEN} # ${WHITE}${1}${NC}"
}
function gn_warn() {
	echo -e "${L_ORANGE} ! ${WHITE}${1}${NC}"
}
function gn_error() {
	echo -e "${L_RED} X ${WHITE}${1}${NC}"
}

function gn_root_check() {
	if [ "$EUID" -ne 0 ]; then
		gn_error "Please run as root"
		exit 1
	else
		gn_log "Welcome! You are ready to run this script!"
	fi
}

#####################################

gn_root_check

gn_log "Checking if Git is installed ..."
which git > /dev/null
if [ "$?" -ne 0 ]; then
    gn_error "Git is not installed on this system! Install git then run the installer again."
    exit 1
fi

gn_log "Checking existing installation ..."
if [ -d "$GN_INSTALL_PATH" ]; then
    gn_error "An existing installation already exists! Aborting installation"
    exit 1
fi

gn_log "Cloning repository ..."
git clone $GIT_URL_HTTP $GN_INSTALL_PATH

exit_code=$?
if [ "$exit_code" -ne 0 ]; then
    gn_error "Failed to clone repository! Exit code $exit_code"
    exit 1
fi

gn_log "Setting scripts to executable ..."
chmod +x ${GN_INSTALL_PATH}/**/*.sh

gn_log "Complete! You can add the use.sh script to your .bashrc (or equivalent) to make the scripts accessible by name."
