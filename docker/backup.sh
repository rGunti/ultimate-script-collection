#!/bin/bash
source "$(dirname $(realpath $0))/../base/base.sh"
gn_hello "Docker Volume Backup"

function print_help() {
    cat << EOF
Usage:
backup.sh <SourceVolume> <DestinationFolder> <ArchiveName>
> TARs the content of the volume <SourceVolume> to <DestinationFolder>
  in a new file with the name "<ArchiveName>_YYYYMMDD_HHmmss.tar.gz"

backup.sh --help
> Shows this help text

backup.sh --list-volumes | -L
> Lists all available volumes

Example:
$ ./backup.sh volume_123 /my/backups volume_123_backup
EOF
}

function list_volumes() {
    docker volume ls -q
}

if [ $# -ne 3 ] || [[ "$1" == "--help" ]]; then
    if [[ "$1" == "--list" ]] || [[ "$1" == "-L" ]]; then
        gn_log "Listing all available Docker volumes:"
        list_volumes
        exit 0
    else
        print_help
        exit 1
    fi
fi

# --- Parameters ---
source_container="$1"
destination_folder=$(gn_sanitize_path "$2")
archive_name="$3"

# --- Actual main ---
# Check if Docker volume exists
gn_debug "Checking if Docker volume \"${source_container}\" exists"
existing_volumes=$(list_volumes)
if ! [[ ${existing_volumes} =~ "${source_container}" ]]; then
    gn_error "Volume \"${source_container}\" doesn't exists"
    exit 1
else
    gn_debug " -> Volume exists"
fi

# Check if destination folder exists
gn_debug "Checking if destination folder \"${destination_folder}\" exists"
if [ ! -d "$destination_folder" ]; then
    gn_log "Destination ${destination_folder} doesn't exist, creating ..."
    mkdir -p "$destination_folder"

    exit_code=$?
    if [[ $exit_code != 0 ]]; then
        gn_error "Couldn't create backup directory (Exit Code ${exit_code}), process aborted"
        exit $exit_code
    fi
else
    gn_debug " -> Destination folder exists"
fi

timestamp=$(date +"%Y%m%d_%H%M%S")
archive_file_name="${archive_name}_${timestamp}.tar.gz"
incontainer_path="/data/dst/${archive_file_name}"
host_path="${destination_folder}/${archive_file_name}"
command="cd /data/src && tar cvzf \"${incontainer_path}\" . && chown ${UID}:${GID} \"${incontainer_path}\""

gn_debug "Will be creating backup at \"${incontainer_path}\" (${host_path})"
gn_debug "Command will be:"
gn_debug " -> ${command}"
gn_log "Starting backup process ..."
docker run --rm \
    -v "$source_container":/data/src \
    -v "$destination_folder":/data/dst \
    alpine sh \
    -c "$command"

exit_code=$?
if [[ $exit_code != 0 ]]; then
    gn_error "Backup process failed with exit code ${exit_code}"
    if [[ -f "${host_path}" ]]; then
        gn_log "Trying to remove created archive ..."
        rm "${host_path}"
    fi
    exit $exit_code
fi

gn_log "Backup complete! Stored at ${host_path}"
