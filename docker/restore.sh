#!/bin/bash
source "$(dirname $(realpath $0))/../base/base.sh"
gn_hello "Docker Volume Restore"

function print_help() {
    cat << EOF
Usage:
restore.sh <SourceTar> <DestinationVolume> [--no-clear]
> UnTARs the content of the <SourceTar> into an existing <DestinationVolume>.
  If --no-clear is provided, the destination volume will not be cleared
  before untaring.

restore.sh --help
> Shows this help text

restore.sh --list-volumes | -L
> Lists all available volumes

Example:
$ ./restore.sh /my/backups/the_backup.tar.gz the_volume
EOF
}

function list_volumes() {
    docker volume ls -q
}

if [ $# -lt 2 ] || [ $# -gt 3 ] || [[ "$1" == "--help" ]]; then
    if [[ "$1" == "--list" ]] || [[ "$1" == "-L" ]]; then
        gn_log "Listing all available Docker volumes:"
        list_volumes
        exit 0
    else
        print_help
        exit 1
    fi
fi

# --- Parameters ---
source_tar=$(gn_sanitize_path "$1")
dest_vol="$2"
no_clear="$3"

if [[ ! -z "${no_clear}" ]] && [[ $no_clear != "--no-clear" ]]; then
    print_help
    exit 1
else
    gn_debug "--no-clear has been enabled"
    no_clear=1
fi

# --- Actual main ---
# Check if Docker volume exists
gn_debug "Checking if Docker volume \"${dest_vol}\" exists"
existing_volumes=$(list_volumes)
if ! [[ ${existing_volumes} =~ "${dest_vol}" ]]; then
    gn_error "Volume \"${dest_vol}\" doesn't exists"
    exit 1
else
    gn_debug " -> Volume exists"
fi

incontainer_src_path="/backup.tar"
incontainer_dst_path="/data"
host_path="${source_tar}"
command="tar -xf \"${incontainer_src_path}\" -C \"${incontainer_dst_path}\""

if [[ ! -z "$no_clear" ]]; then
    command="rm -rf /data/* && ${command}"
fi

gn_debug "Command will be:"
gn_debug " -> ${command}"
gn_log "Starting restore process ..."
docker run --rm \
    -v "${source_tar}:${incontainer_src_path}:ro" \
    -v "${dest_vol}:${incontainer_dst_path}" \
    alpine sh \
    -c "$command"

exit_code=$?
if [[ $exit_code != 0 ]]; then
    gn_error "Restore process failed with exit code ${exit_code}"
    exit $exit_code
fi

gn_log "Restore complete!"
